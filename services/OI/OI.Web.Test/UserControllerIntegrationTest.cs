using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using OI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OI.Web.Test
{
    public class UserControllerIntegrationTest : IClassFixture<WebApplicationFactory<OI.Web.Startup>>
    {
        private readonly WebApplicationFactory<OI.Web.Startup> _factory;

        public UserControllerIntegrationTest(WebApplicationFactory<OI.Web.Startup> factory) => _factory = factory;

        [Fact]
        public async Task Test1UserGetList()
        {
            using var client = _factory.CreateClient();

            var response = await client.GetAsync("users");
            response.EnsureSuccessStatusCode();

            var users = JsonConvert.DeserializeObject<IList<User>>(response.Content.ReadAsStringAsync().Result);
            Assert.Equal(12, users.Count);

            Assert.Equal("Leanne Graham", users[0].Name);
            Assert.NotNull(users[0].Address);
            Assert.Equal("Gwenborough", users[0].Address.City);
        }
        [Fact]
        public async Task Test2UserGetSingle()
        {
            using var client = _factory.CreateClient();

            var response = await client.GetAsync("users/1");
            response.EnsureSuccessStatusCode();

            var user = JsonConvert.DeserializeObject<User>(response.Content.ReadAsStringAsync().Result);
            Assert.NotNull(user);

            Assert.Equal("Leanne Graham", user.Name);
            Assert.NotNull(user.Address);
            Assert.Equal("Gwenborough", user.Address.City);
        }

        [Fact]
        public async Task Test3UserCreate()
        {
            using var client = _factory.CreateClient();

            var body = JsonConvert.SerializeObject(
                new User {
                    Name = "CreateTest"
                    , Address = new Address {
                        City = "Test city"
                    }
                });

            var content = new StringContent(body, Encoding.UTF8, "application/json");

            var response = await client.PostAsync("users", content);
            response.EnsureSuccessStatusCode();

            var user = JsonConvert.DeserializeObject<User>(response.Content.ReadAsStringAsync().Result);
            Assert.NotNull(user);

            Assert.Equal(11, user.Id);
            Assert.Equal("CreateTest", user.Name);
            Assert.NotNull(user.Address);
            Assert.Equal("Test city", user.Address.City);
        }

        [Fact]
        public async Task Test4UserDelete()
        {
            using var client = _factory.CreateClient();

            var body = JsonConvert.SerializeObject(
                new User
                {
                    Name = "CreateTest"
                    ,
                    Address = new Address
                    {
                        City = "Test city"
                    }
                });

            var content = new StringContent(body, Encoding.UTF8, "application/json");

            var response = await client.PostAsync("users", content);
            response.EnsureSuccessStatusCode();

            var user = JsonConvert.DeserializeObject<User>(response.Content.ReadAsStringAsync().Result);

            response = await client.DeleteAsync($"users/{user.Id}");
            response.EnsureSuccessStatusCode();

            response = await client.GetAsync($"users/{user.Id}");
            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task Test5UserUpdate()
        {
            using var client = _factory.CreateClient();

            var body = JsonConvert.SerializeObject(
                new User
                {
                    Name = "UpdateTest"
                    ,
                    Address = new Address
                    {
                        City = "Test city"
                    }
                });

            var content = new StringContent(body, Encoding.UTF8, "application/json");

            var response = await client.PostAsync("users", content);
            response.EnsureSuccessStatusCode();

            var user = JsonConvert.DeserializeObject<User>(response.Content.ReadAsStringAsync().Result);

            user.Name = "UpdatedName";
            user.Address.City = "UpdatedCity";

            body = JsonConvert.SerializeObject( user );

            content = new StringContent(body, Encoding.UTF8, "application/json");

            response = await client.PutAsync($"users/{user.Id}", content);
            response.EnsureSuccessStatusCode();

            response = await client.GetAsync($"users/{user.Id}");
            response.EnsureSuccessStatusCode();

            user = JsonConvert.DeserializeObject<User>(response.Content.ReadAsStringAsync().Result);

            Assert.NotNull(user);

            Assert.Equal("UpdatedName", user.Name);
            Assert.NotNull(user.Address);
            Assert.Equal("UpdatedCity", user.Address.City);
        }
    }
}
