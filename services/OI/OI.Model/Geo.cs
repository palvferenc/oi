﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OI.Model
{
    public class Geo
    {
        public string Lat { get; set; }
        public string Lng { get; set; }
    }
}
