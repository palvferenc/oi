﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace OI.Model
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.Int64)]
        public Int64 Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public Address Address { get; set; }
        public string Phone { get; set; }
        public string  Website { get; set; }
        public Company Company { get; set; }

    }
}
