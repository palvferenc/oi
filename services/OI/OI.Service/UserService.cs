﻿using OI.Model;
using OI.Repository.Contract;
using OI.Service.Contract;
using System;
using System.Collections.Generic;

namespace OI.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;

        public UserService(IUserRepository repository) => _repository = repository;

        public User Create(User newUser)
        {
            return _repository.Create(newUser);
        }

        public IEnumerable<User> Get()
        {
            return _repository.Get();
        }

        public User Get(long id)
        {
            return _repository.Get(id);
        }

        public void Remove(User toRemove)
        {
            _repository.Remove(toRemove);
        }

        public void Remove(long toRemoveId)
        {
            _repository.Remove(toRemoveId);
        }

        public void Update(long toUpdateId, User toUpdate)
        {
            _repository.Update(toUpdateId, toUpdate);
        }
    }
}
