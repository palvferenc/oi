﻿using OI.Model;
using System;
using System.Collections.Generic;

namespace OI.Service.Contract
{
    public interface IUserService
    {
        IEnumerable<User> Get();
        User Get(Int64 id);
        User Create(User newUser);
        void Update(Int64 toUpdateId, User toUpdate);
        void Remove(User toRemove);
        void Remove(Int64 toRemoveId);
    }
}
