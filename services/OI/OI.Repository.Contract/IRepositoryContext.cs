﻿using MongoDB.Driver;
using OI.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OI.Repository.Contract
{
    public interface IRepositoryContext
    {
        IMongoDatabase Database { get; }
        IMongoCollection<User> Users { get; }
    }
}
