﻿using OI.Model;
using System;
using System.Collections.Generic;

namespace OI.Repository.Contract
{
    public interface IUserRepository
    {
        IEnumerable<User> Get();
        User Get(Int64 id);
        User Create(User newUser);
        void Update(Int64 toUpdateId, User source);
        void Remove(User toRemove);
        void Remove(Int64 toRemoveId);

    }
}
