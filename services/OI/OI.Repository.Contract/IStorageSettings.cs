﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OI.Repository.Contract
{
    public interface IStorageSettings
    {
        public string ConnectionString { get; set; }
        public string UsersCollectionName { get; set; }
        public string DatabaseName { get; set; }
    }
}
