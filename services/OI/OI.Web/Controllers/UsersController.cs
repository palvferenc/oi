﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OI.Model;
using OI.Repository.Contract;
using OI.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OI.Web.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        ILogger<UsersController> _logger;
        private readonly IUserService _userService;
        public UsersController(ILogger<UsersController> logger,IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        // GET: api/<UsersController>
        [HttpGet]
        public ActionResult<IEnumerable<User>> Get() => Ok(_userService.Get());

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        public ActionResult<User> Get(long id)
        {
            var user = _userService.Get(id);
            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // POST api/<UsersController>
        [HttpPost]
        public ActionResult<User> Post([FromBody] User value)
        {
            return _userService.Create(value);
        }

        // PUT api/<UsersController>/5
        [HttpPut("{id}")]
        public IActionResult Put(long id, [FromBody] User value)
        {
            try
            {
                _userService.Update(id, value);
            }
            catch (DocumentNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            try
            {
                _userService.Remove(id);
            }
            catch (DocumentNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
