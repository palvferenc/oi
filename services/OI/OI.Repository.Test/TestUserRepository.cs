﻿using MongoDB.Driver;
using OI.Model;
using OI.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OI.Repository.Test
{
    public class TestUserRepository
    {
        [Fact]
        public void TestUserGetSingle()
        {
            var context = new RepositoryContext(new StorageSettings {
                ConnectionString = "mongodb://localhost:27017"
                ,
                DatabaseName = "OITestUserRepoGetSingle"
                ,
                UsersCollectionName = "Users"
            });

            IUserRepository underTest = new UserRepository(context);

            var user = underTest.Get(1);

            Assert.NotNull(user);
            Assert.NotNull(user.Name);
            Assert.NotNull(user.Address.City);

            Assert.Equal(1, user.Id);
            Assert.Equal("Leanne Graham", user.Name);
            Assert.Equal("Gwenborough", user.Address.City);
        }

        [Fact]
        public void TestUserGeList()
        {
            var context = new RepositoryContext(new StorageSettings
            {
                ConnectionString = "mongodb://localhost:27017"
                ,
                DatabaseName = "OITestUserRepoGetList"
                ,
                UsersCollectionName = "Users"
            });

            IUserRepository underTest = new UserRepository(context);

            var users = underTest.Get().ToList();

            Assert.NotNull(users);
            Assert.NotNull(users[0].Name);
            Assert.NotNull(users[0].Address.City);

            Assert.NotNull(users[9].Name);
            Assert.NotNull(users[9].Address.City);


            Assert.Equal(10, users.Count);
            Assert.Equal(1, users[0].Id);
            Assert.Equal(10, users[9].Id);

            Assert.Equal("Leanne Graham", users[0].Name);
            Assert.Equal("Clementina DuBuque", users[9].Name);

            Assert.Equal("Gwenborough", users[0].Address.City);
            Assert.Equal("Lebsackbury", users[9].Address.City);
        }

        [Fact]
        public void TestUserCreate()
        {
            var context = new RepositoryContext(new StorageSettings
            {
                ConnectionString = "mongodb://localhost:27017"
                ,
                DatabaseName = "OITestUserRepoCreate"
                ,
                UsersCollectionName = "Users"
            });

            IUserRepository underTest = new UserRepository(context);

            var user = underTest.Create(new User { Name = "Test1", Address = new Address { City = "City" } });

            Assert.NotNull(user);
            Assert.NotNull(user.Name);
            Assert.NotNull(user.Address.City);

            Assert.Equal(11, user.Id);
            Assert.Equal("Test1", user.Name);
            Assert.Equal("City", user.Address.City);

            user = context.Users.Find<User>(user => user.Id == 11).FirstOrDefault();

            Assert.NotNull(user);
            Assert.NotNull(user.Name);
            Assert.NotNull(user.Address.City);

            Assert.Equal(11, user.Id);
            Assert.Equal("Test1", user.Name);
            Assert.Equal("City", user.Address.City);

        }

        [Fact]
        public void TestUserRemove()
        {
            var context = new RepositoryContext(new StorageSettings
            {
                ConnectionString = "mongodb://localhost:27017"
                ,
                DatabaseName = "OITestUserRepoRemove"
                ,
                UsersCollectionName = "Users"
            });

            IUserRepository underTest = new UserRepository(context);

            underTest.Remove(new User { Id = 1});

            Assert.Null( context.Users.Find<User>(user => user.Id == 1).FirstOrDefault());
        }

        [Fact]
        public void TestUserRemoveById()
        {
            var context = new RepositoryContext(new StorageSettings
            {
                ConnectionString = "mongodb://localhost:27017"
                ,
                DatabaseName = "OITestUserRepoRemoveById"
                ,
                UsersCollectionName = "Users"
            });

            IUserRepository underTest = new UserRepository(context);

            underTest.Remove(1);

            Assert.Null(context.Users.Find<User>(user => user.Id == 1).FirstOrDefault());
        }

        [Fact]
        public void TestUserUpdate()
        {
            var context = new RepositoryContext(new StorageSettings
            {
                ConnectionString = "mongodb://localhost:27017"
                ,
                DatabaseName = "OITestUserRepoUpdate"
                ,
                UsersCollectionName = "Users"
            });

            IUserRepository underTest = new UserRepository(context);

            var user = context.Users.Find<User>(user => user.Id == 1).FirstOrDefault();
            user.Name = "test";

            underTest.Update(user.Id, user);

            user = context.Users.Find<User>(user => user.Id == 1).FirstOrDefault();

            Assert.NotNull(user);
            Assert.Equal("test", user.Name);
            Assert.NotNull(user.Address);
            Assert.Equal("Gwenborough", user.Address.City);
        }

        [Fact]
        public void TestUserUpdateThrowDocumentNotFound()
        {
            var context = new RepositoryContext(new StorageSettings
            {
                ConnectionString = "mongodb://localhost:27017"
                ,
                DatabaseName = "OITestUserRepoUpdate"
                ,
                UsersCollectionName = "Users"
            });

            IUserRepository underTest = new UserRepository(context);

            var user = new User();

            Action testCode = () => { underTest.Update(111, user); };

            Assert.Throws<DocumentNotFoundException>(testCode);
        }
    }
}
