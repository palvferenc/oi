using MongoDB.Driver;
using OI.Model;
using OI.Repository.Contract;
using System;
using Xunit;

namespace OI.Repository.Test
{
    public class TestRepositoryContext
    {
        [Fact]
        public void TestDatabaseInitialization()
        {
            IStorageSettings settings = new StorageSettings()
            {
                ConnectionString = "mongodb://localhost:27017"
                , DatabaseName = "OITestContext"
                , UsersCollectionName = "Users"
            };

            IRepositoryContext context = new RepositoryContext(settings);

            var usersCollection = context.Database.GetCollection<User>(settings.UsersCollectionName);

            Assert.NotNull(usersCollection);

            Assert.Equal(10, usersCollection.CountDocuments<User>(user => true));
        }
    }
}
