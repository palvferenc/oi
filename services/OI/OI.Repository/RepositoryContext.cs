﻿using MongoDB.Driver;
using OI.Model;
using OI.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace OI.Repository
{
    public class RepositoryContext : IRepositoryContext
    {
        private readonly IStorageSettings _settings;
        private readonly IMongoClient _client;
        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<User> _users;

        public RepositoryContext(IStorageSettings settings) {
            _settings = settings;
            _client = new MongoClient(settings.ConnectionString);
            _database = _client.GetDatabase(settings.DatabaseName);

            InitializeSchema();

            _users = _database.GetCollection<User>(settings.UsersCollectionName);

            AddDefaultUsers();
        }

        public IMongoDatabase Database => _database ;

        public IMongoCollection<User> Users => _users;

        private void InitializeSchema()
        {
            _database.DropCollection(_settings.UsersCollectionName);
            _database.CreateCollection(_settings.UsersCollectionName);
        }

        private void AddDefaultUsers()
        {
            Users.InsertOne(new User {
                Id = 1
                , Name = "Leanne Graham"
                , Username = "Bret"
                , Email = "Sincere@april.biz"
                , Address = new Address {
                    Street = "Kulas Light"
                    , Suite = "Apt. 556"
                    , City = "Gwenborough"
                    , ZipCode = "92998-3874"
                    , Geo = new Geo
                    {
                        Lat = "-37.3159"
                        , Lng = "81.1496"
                    }
                }
                , Phone = "1-770-736-8031 x56442"
                , Website = "hildegard.org"
                , Company = new Company {
                    Name = "Romaguera - Crona"
                    , CatchPhrase = "Multi-layered client-server neural-net"
                    , Bs = "harness real-time e-markets"
                }
            });

            Users.InsertOne(new User
            {
                Id = 2
                ,
                Name = "Ervin Howell"
                ,
                Username = "Antonette"
                ,
                Email = "Shanna@melissa.tv"
                ,
                Address = new Address
                {
                    Street = "Victor Plains"
                    ,
                    Suite = "Suite 879"
                    ,
                    City = "Wisokyburgh"
                    ,
                    ZipCode = "90566-7771"
                    ,
                    Geo = new Geo
                    {
                        Lat = "-43.9509"
                        ,
                        Lng = "-34.4618"
                    }
                }
                ,
                Phone = "010-692-6593 x09125"
                ,
                Website = "anastasia.net"
                ,
                Company = new Company
                {
                    Name = "Deckow-Crist"
                    ,
                    CatchPhrase = "Proactive didactic contingency"
                    ,
                    Bs = "synergize scalable supply-chains"
                }
            });

            Users.InsertOne(new User
            {
                Id = 3
                ,
                Name = "Clementine Bauch"
                ,
                Username = "Samantha"
                ,
                Email = "Nathan@yesenia.net"
                ,
                Address = new Address
                {
                    Street = "Douglas Extension"
                    ,
                    Suite = "Suite 847"
                    ,
                    City = "McKenziehaven"
                    ,
                    ZipCode = "59590-4157"
                    ,
                    Geo = new Geo
                    {
                        Lat = "-68.6102"
                        ,
                        Lng = "-47.0653"
                    }
                }
                ,
                Phone = "1-463-123-4447"
                ,
                Website = "ramiro.info"
                ,
                Company = new Company
                {
                    Name = "Romaguera-Jacobson"
                    ,
                    CatchPhrase = "Face to face bifurcated interface"
                    ,
                    Bs = "e-enable strategic applications"
                }
            });

            Users.InsertOne(new User
            {
                Id = 4
                ,
                Name = "Patricia Lebsack"
                ,
                Username = "Karianne"
                ,
                Email = "Julianne.OConner@kory.org"
                ,
                Address = new Address
                {
                    Street = "Hoeger Mall"
                    ,
                    Suite = "Apt. 692"
                    ,
                    City = "South Elvis"
                    ,
                    ZipCode = "53919-4257"
                    ,
                    Geo = new Geo
                    {
                        Lat = "29.4572"
                        ,
                        Lng = "-164.2990"
                    }
                }
                ,
                Phone = "493-170-9623 x156"
                ,
                Website = "kale.biz"
                ,
                Company = new Company
                {
                    Name = "Robel-Corkery"
                    ,
                    CatchPhrase = "Multi-tiered zero tolerance productivity"
                    ,
                    Bs = "transition cutting-edge web services"
                }
            });

            Users.InsertOne(new User
            {
                Id = 5
                ,
                Name = "Chelsey Dietrich"
                ,
                Username = "Kamren"
                ,
                Email = "Lucio_Hettinger@annie.ca"
                ,
                Address = new Address
                {
                    Street = "Skiles Walks"
                    ,
                    Suite = "Suite 351"
                    ,
                    City = "Roscoeview"
                    ,
                    ZipCode = "33263"
                    ,
                    Geo = new Geo
                    {
                        Lat = "-31.8129"
                        ,
                        Lng = "62.5342"
                    }
                }
                ,
                Phone = "(254)954-1289"
                ,
                Website = "demarco.info"
                ,
                Company = new Company
                {
                    Name = "Keebler LLC"
                    ,
                    CatchPhrase = "User-centric fault-tolerant solution"
                    ,
                    Bs = "revolutionize end-to-end systems"
                }
            });

            Users.InsertOne(new User
            {
                Id = 6
                ,
                Name = "Mrs. Dennis Schulist"
                ,
                Username = "Leopoldo_Corkery"
                ,
                Email = "Karley_Dach@jasper.info"
                ,
                Address = new Address
                {
                    Street = "Norberto Crossing"
                    ,
                    Suite = "Apt. 950"
                    ,
                    City = "South Christy"
                    ,
                    ZipCode = "23505-1337"
                    ,
                    Geo = new Geo
                    {
                        Lat = "-71.4197"
                        ,
                        Lng = "71.7478"
                    }
                }
                ,
                Phone = "1-477-935-8478 x6430"
                ,
                Website = "ola.org"
                ,
                Company = new Company
                {
                    Name = "Considine-Lockman"
                    ,
                    CatchPhrase = "Synchronised bottom-line interface"
                    ,
                    Bs = "e-enable innovative applications"
                }
            });

            Users.InsertOne(new User
            {
                Id = 7
                ,
                Name = "Kurtis Weissnat"
                ,
                Username = "Elwyn.Skiles"
                ,
                Email = "Telly.Hoeger@billy.biz"
                ,
                Address = new Address
                {
                    Street = "Rex Trail"
                    ,
                    Suite = "Suite 280"
                    ,
                    City = "Howemouth"
                    ,
                    ZipCode = "58804-1099"
                    ,
                    Geo = new Geo
                    {
                        Lat = "24.8918"
                        ,
                        Lng = "21.8984"
                    }
                }
                ,
                Phone = "210.067.6132"
                ,
                Website = "elvis.io"
                ,
                Company = new Company
                {
                    Name = "Johns Group"
                    ,
                    CatchPhrase = "Configurable multimedia task-force"
                    ,
                    Bs = "generate enterprise e-tailers"
                }
            });

            Users.InsertOne(new User
            {
                Id = 8
                ,
                Name = "Nicholas Runolfsdottir V"
                ,
                Username = "Maxime_Nienow"
                ,
                Email = "Sherwood@rosamond.me"
                ,
                Address = new Address
                {
                    Street = "Ellsworth Summit"
                    ,
                    Suite = "Suite 729"
                    ,
                    City = "Aliyaview"
                    ,
                    ZipCode = "45169"
                    ,
                    Geo = new Geo
                    {
                        Lat = "-14.3990"
                        ,
                        Lng = "-120.7677"
                    }
                }
                ,
                Phone = "586.493.6943 x140"
                ,
                Website = "jacynthe.com"
                ,
                Company = new Company
                {
                    Name = "Abernathy Group"
                    ,
                    CatchPhrase = "Implemented secondary concept"
                    ,
                    Bs = "e-enable extensible e-tailers"
                }
            });

            Users.InsertOne(new User
            {
                Id = 9
                ,
                Name = "Glenna Reichert"
                ,
                Username = "Delphine"
                ,
                Email = "Chaim_McDermott@dana.io"
                ,
                Address = new Address
                {
                    Street = "Dayna Park"
                    ,
                    Suite = "Suite 449"
                    ,
                    City = "Bartholomebury"
                    ,
                    ZipCode = "76495-3109"
                    ,
                    Geo = new Geo
                    {
                        Lat = "24.6463"
                        ,
                        Lng = "-168.8889"
                    }
                }
                ,
                Phone = "(775)976-6794 x41206"
                ,
                Website = "conrad.com"
                ,
                Company = new Company
                {
                    Name = "Yost and Sons"
                    ,
                    CatchPhrase = "Switchable contextually-based project"
                    ,
                    Bs = "aggregate real-time technologies"
                }
            });

            Users.InsertOne(new User
            {
                Id = 10
                ,
                Name = "Clementina DuBuque"
                ,
                Username = "Moriah.Stanton"
                ,
                Email = "Rey.Padberg@karina.biz"
                ,
                Address = new Address
                {
                    Street = "Kattie Turnpike"
                    ,
                    Suite = "Suite 198"
                    ,
                    City = "Lebsackbury"
                    ,
                    ZipCode = "31428-2261"
                    ,
                    Geo = new Geo
                    {
                        Lat = "-38.2386"
                        ,
                        Lng = "57.2232"
                    }
                }
                ,
                Phone = "024-648-3804"
                ,
                Website = "ambrose.net"
                ,
                Company = new Company
                {
                    Name = "Hoeger LLC"
                    ,
                    CatchPhrase = "Centralized empowering task-force"
                    ,
                    Bs = "target end-to-end models"
                }
            });
        }
    }
}
