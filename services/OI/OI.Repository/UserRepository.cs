﻿using MongoDB.Driver;
using OI.Model;
using OI.Repository.Contract;
using System;
using System.Collections.Generic;

namespace OI.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IRepositoryContext _context;

        public UserRepository(IRepositoryContext context) => _context = context;

        public User Create(User newUser)
        {
            var user = _context.Users.Find<User>(user => true).SortByDescending(user => user.Id).FirstOrDefault();
            newUser.Id = user != null ? user.Id + 1 : 1;
            _context.Users.InsertOne(newUser);
            return newUser;
        }

        public IEnumerable<User> Get() => _context.Users.Find<User>( user => true).ToList();

        public User Get(long id) => _context.Users.Find<User>(user => user.Id == id).FirstOrDefault();

        public void Remove(User toRemove) {
            if (Get(toRemove.Id) == null)
            {
                throw new DocumentNotFoundException($"Cannot find user with id: {toRemove.Id} in the database");
            }
            _context.Users.DeleteOne(user => user.Id == toRemove.Id);
        }

        public void Remove(long toRemoveId) {
            if (Get(toRemoveId) == null)
            {
                throw new DocumentNotFoundException($"Cannot find user with id: {toRemoveId} in the database");
            }
            _context.Users.DeleteOne(user => user.Id == toRemoveId);
        }

        public void Update(long toUpdateId, User source) {
            if ( Get(toUpdateId) == null)
            {
                throw new DocumentNotFoundException($"Cannot find user with id: {toUpdateId} in the database");
            }
            _context.Users.ReplaceOne(user => user.Id == toUpdateId, source);
        }
    }
}
