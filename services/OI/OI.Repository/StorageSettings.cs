﻿using OI.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace OI.Repository
{
    public class StorageSettings : IStorageSettings
    {
        public string ConnectionString { get ; set ; }
        public string UsersCollectionName { get ; set ; }
        public string DatabaseName { get ; set ; }
    }
}
