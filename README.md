# OI User Management API

## Requirements

- .NET Core 3.1
- Docker desktop on Windows, docker ce, docker compose on Linux
- Visual Studio/Jetbrains Rider

## Containers

Please use the provided docker compose configuration for development, there are mongodb and mongo express instances, mongoexpress: localhost:8081

## Test
All layer has test project in the solution, OI.Web.Test contains the integrations tests